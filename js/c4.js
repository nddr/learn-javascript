document.addEventListener('DOMContentLoaded', async () => {
  const pokeArray = [];
  const pokemons = await fetch('https://pokeapi.co/api/v2/pokemon?limit=10')
    .then((res) => res.json())
    .then((data) => {
      return data;
    });

  for (const pokemon of pokemons.results) {
    await fetch(pokemon.url)
      .then((res) => res.json())
      .then((data) => {
        pokeArray.push(data);
      });
  }

  pokeArray.forEach((pokemon) => {
    const pokedex = document.querySelector('.pokedex');
    const card = document.createElement('div');
    card.classList.add('card');
    card.innerHTML = `
      <img src="${pokemon.sprites.front_default}">
      <div class="name">${pokemon.name}</div>
    `;

    pokedex.appendChild(card);
  });
});