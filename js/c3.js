document.addEventListener('DOMContentLoaded', function () {
  const mole = document.querySelector('.mole');
  const button = document.querySelector('.button');
  const scoreEl = document.querySelector('.score');
  let start = false;
  let score = 0;
  let gameLoop;

  button.addEventListener('click', function () {
    start = !start;
    if (start) {
      startGame();
      button.style.backgroundColor = '#a32f2f';
      button.textContent = 'Stop';
    }
    else {
      score = 0;
      scoreEl.textContent = score;
      clearInterval(gameLoop);
      button.style.backgroundColor = '#2fa367';
      button.textContent = 'Start';
    }
  });

  mole.addEventListener('click', function () {
    score++;
    scoreEl.textContent = score;

    mole.style.display = 'none';

    if (score >= 6) {
      alert('Sheeesh!! You win!');
      score = 0;
      scoreEl.textContent = score;
      clearInterval(gameLoop);
      button.style.backgroundColor = '#2fa367';
      button.textContent = 'Start';
    }
  });

  const startGame = () => {
    gameLoop = setInterval(() => {
      mole.style.display = 'block';
      mole.style.left = Math.floor(Math.random() * 800 - 60) + 'px';
      mole.style.top = Math.floor(Math.random() * 400 - 60) + 'px';

      setTimeout(function () {
        mole.style.display = 'none';
      }, 1500);
    }, 3000);
  };
});