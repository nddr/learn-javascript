document.addEventListener('DOMContentLoaded', function () {
  const menu = document.querySelector('.menu');
  const nav = document.querySelector('.nav-container');
  menu.addEventListener('click', function (e) {
    nav.classList.toggle('open');
  });

  document.addEventListener('click', function (e) {
    if (e.target === menu) return false;
    e.target.closest('.nav-container') || nav.classList.remove('open');
  });
});