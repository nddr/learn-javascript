document.addEventListener('DOMContentLoaded', function () {
  const box1 = document.getElementById('box1');
  box1.addEventListener('click', function (e) {
    e.target.classList.add('grow');
  });

  const box2 = document.getElementById('box2');
  box2.addEventListener('click', function (e) {
    e.target.classList.add('grow');
  });

  const box3 = document.getElementById('box3');
  box3.addEventListener('click', function (e) {
    e.target.classList.add('grow');
  });
});